# Copyright (c) 2022-2023, Ora Lassila & So Many Aircraft
# All rights reserved.
#
# See LICENSE for licensing information
#
import json
import sys
import unittest
import rdflib
from src.tinyrml import Mapper

EX = rdflib.Namespace("https:/somanyaircraft.com/test#")

class BasicTests(unittest.TestCase):

    def setUp(self) -> None:
        self.mapper = Mapper("./test-map.ttl")

    def test_setup(self):
        self.assertTrue(isinstance(self.mapper, Mapper))
        self.assertTrue(isinstance(self.mapper.graph, rdflib.Graph))

    def test_simple_map(self):
        rows = [{"id": "one", "foo": 7, "bar": "baz", "baz": None}]
        result = self.mapper.process(rows)
        type_triples = list(result.triples((None, rdflib.RDF.type, EX.Foo)))
        self.assertTrue(len(type_triples) == 1)
        self.assertTrue(type_triples[0][0] == EX.one)
        foo_triples = list(result.triples((EX.one, EX.foo, None)))
        self.assertTrue(len(foo_triples) == 1)
        self.assertTrue(foo_triples[0][2].value == 14)
        bar_triples = list(result.triples((EX.one, EX.bar, None)))
        self.assertTrue(len(bar_triples) == 1)
        self.assertTrue(bar_triples[0][2] == rdflib.Literal("baz"))

    def test_empty_string_value(self):
        rows = [{"id": "one", "foo": 7, "bar": "", "baz": None}]
        result = self.mapper.process(rows)
        triples = list(result.triples((None, EX.bar, None)))
        self.assertTrue(len(triples) == 0)
        mapper = Mapper("./test-map.ttl", empty_string_is_none=False)
        result = mapper.process(rows)
        triples = list(result.triples((None, EX.bar, None)))
        self.assertTrue(len(triples) == 1)
        self.assertTrue(str(triples[0][2]) == "")

    def test_no_expressions(self):
        with self.assertRaises(ValueError):
            mapper = Mapper("./test-map.ttl", allow_expressions=False)

    def test_constants(self):
        rows = [{"id": "one", "foo": 7, "bar": "baz", "baz": "1"}]
        result = self.mapper.process(rows)
        const_triples = list(result.triples((EX.one, EX.const, None)))
        self.assertTrue(len(const_triples) == 1)
        self.assertTrue(const_triples[0][2] == EX.something)

    def test_expand_as_list(self):
        rows = [{"id": "one", "foo": 7, "bar": "baz", "baz": "1, 2"}]
        result = self.mapper.process(rows)
        self.assertTrue(len(result) == 8)
        baz_triples = list(result.triples((EX.one, EX.baz, None)))
        self.assertTrue(len(baz_triples) == 2)

class BNodeSubjectTests(unittest.TestCase):

    def setUp(self) -> None:
        self.mapper = Mapper("./test-map-bnode-subject.ttl")

    def test_bnode_subject(self):
        result = self.mapper.process([{"foo": 1}])
        self.assertTrue(len(result) == 1)
        triple = list(result.triples((None, None, None)))[0]
        self.assertTrue(isinstance(triple[0], rdflib.BNode))
        self.assertTrue(triple[1] == EX.foo)
        self.assertTrue(triple[2].value == 1)

class BasicTestsFunkyNames(unittest.TestCase):

    def setUp(self) -> None:
        self.mapper = Mapper("./test-map-funky-names.ttl")

    def test_simple_map_funky_names(self):
        rows = [{"id": "one", "foo": 7, "bar": "baz", "baz": None}]
        result = self.mapper.process(rows)
        type_triples = list(result.triples((None, rdflib.RDF.type, EX.Foo)))
        self.assertTrue(len(type_triples) == 1)
        self.assertTrue(type_triples[0][0] == EX.one)
        foo_triples = list(result.triples((EX.one, EX.foo, None)))
        self.assertTrue(len(foo_triples) == 1)
        self.assertTrue(foo_triples[0][2].value == 14)
        bar_triples = list(result.triples((EX.one, EX.bar, None)))
        self.assertTrue(len(bar_triples) == 1)
        self.assertTrue(bar_triples[0][2] == rdflib.Literal("baz"))

class JSONPathTests(unittest.TestCase):

    def setUp(self) -> None:
        self.mapper = Mapper("./test-map-json.ttl", input_is_json=True)

    def test_flatten(self):
        self.assertTrue(Mapper.flatten({}) == {})
        self.assertTrue(Mapper.flatten(json.loads('{"a": {"b": 1}}')) == {"a.b": 1})

    def test_json_access(self):
        rows = [json.loads('{"id": 0, "a": {"b": 1}}')]
        result = self.mapper.process(rows)
        self.assertTrue(len(result) == 2)
        for s, p, o in result.triples((EX["0"], EX.bar, None)):
            self.assertTrue(o.value == 1)

    def test_missing_value(self):
        rows = [json.loads('{"id": 0, "a": {}}')]
        with self.assertRaises(KeyError):
            self.mapper.process(rows)

    def test_non_json_processing(self):
        mapper = Mapper("./test-map-json.ttl", input_is_json=False)
        with self.assertRaises(KeyError):
            rows = [json.loads('{"id": 0, "a": {"b": 1}}')]
            mapper.process(rows)

class CSVTests(unittest.TestCase):

    def setUp(self) -> None:
        self.mapper = Mapper("./test-csv-map.ttl")

    def test_csv_processing(self):
        g = self.mapper.processCSVFile("./test-csv.csv", skip_unicode_marker=False)
        g.bind("ex", EX)
        self.assertTrue(len(g) == 4)
        results = {b.value for b, in g.query("SELECT ?b { ?a a ex:Foo ; ex:bar ?b }")}
        self.assertTrue(results == {"xxx", "yyy"})

class ErrorTests(unittest.TestCase):

    def test_logical_table_fail(self):
        with self.assertRaises(ValueError) as e:
            Mapper("./test-logical-table-map.ttl").process([])
            self.assertTrue(str(e).startswith("TinyRML does not handle rr:logicalTable"))

    def test_logical_table_warn(self):
        Mapper("./test-logical-table-map.ttl", failForLogicalTables=False).process([])
        self.assertTrue(True)

    def test_triples_map_not_found(self):
        with self.assertRaises(ValueError) as e:
            Mapper("./test-map.ttl", triples_map_uri=EX.NonexistentTestMapping)
            self.assertTrue(str(e).startswith("No rr:TriplesMap"))

    def test_triples_map_found(self):
        Mapper("./test-map.ttl", triples_map_uri=EX.TestMapping).process([])
        self.assertTrue(True)
